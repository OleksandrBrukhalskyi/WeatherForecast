var express = require('express')
var app = express()
var request = require('request');
var bodyParser = require('body-parser');
app.set('view engine','ejs');
app.use(bodyParser.urlencoded({ extended : false}));
var publicDir = require('path').join(__dirname,'/public');
app.use(express.static(publicDir));



let url=`http://api.openweathermap.org/data/2.5/forecast?q=Chernivtsi&appid=38e7210d8fc5b0f486498698da1ca5fe`;
app.get('/', function(req, res){

  request(url, function(error, responce, body){
    weather_json = JSON.parse(body);

try{
    var weather ={
      //------------Curr------------//
      city: weather_json.city.name,
      temperature:Math.round(weather_json.list[0].main.temp)-273,
      max_temp : Math.round(weather_json.list[0].main.temp_max)-273.00,
      min_temp : Math.round(weather_json.list[0].main.temp_min)-273,
      description: weather_json.list[0].weather[0].main,
      icon : weather_json.list[0].weather[0].icon,

      //----------------------------------------------------//
      mon_temp : Math.round(weather_json.list[6].main.temp)-273,
      mon_max_temp : Math.round(weather_json.list[6].main.temp_max)-273,
      mon_min_temp : Math.round(weather_json.list[6].main.temp_min)-273,
      mon_desc  : weather_json.list[6].weather[0].description,
      mon_icon : weather_json.list[6].weather[0].icon,
      //------------------------------------------------------//

      thur_temp: Math.round(weather_json.list[14].main.temp)-273,
      thur_max_temp : Math.round(weather_json.list[14].main.temp_max)-273,
      thur_min_temp : Math.round(weather_json.list[14].main.temp_min)-273,
      thur_desc :  weather_json.list[14].weather[0].description,
      thur_icon:   weather_json.list[14].weather[0].icon,
      //-----------------------------------------------------------------------------------//
      wen_temp: Math.round(weather_json.list[22].main.temp)-273,
      wen_max_temp :Math.round(weather_json.list[22].main.temp_max)-273,
      wen_min_temp : Math.round(weather_json.list[22].main.temp_min)-273,
      wen_desc :  weather_json.list[22].weather[0].description,
      wen_icon:   weather_json.list[22].weather[0].icon,
      //-----------------------------------------------------------------------------------//
      tues_temp: Math.round(weather_json.list[30].main.temp)-273,
      tues_max_temp : Math.round(weather_json.list[30].main.temp_max)-273,
      tues_min_temp : Math.round(weather_json.list[30].main.temp_min)-273,
      tues_desc :  weather_json.list[30].weather[0].description,
      tues_icon:   weather_json.list[30].weather[0].icon,
      //-----------------------------------------------------------------------------------//
      fri_temp: Math.round(weather_json.list[38].main.temp)-273,
      fri_max_temp :Math.round(weather_json.list[38].main.temp_max)-273,
      fri_min_temp : Math.round(weather_json.list[38].main.temp_min)-273,
      fri_desc :  weather_json.list[38].weather[0].description,
      fri_icon:   weather_json.list[38].weather[0].icon
    }
  }
    catch(e){
      res.render('404');
      console.log(e);
    }
    res.render('index',{weather: weather});
  });


  });


app.post('/find',function(req,res,next){

  let city = req.body.city;
  let url    = 'http://api.openweathermap.org/data/2.5/forecast?q=';
  let appId  = 'appid=38e7210d8fc5b0f486498698da1ca5fe';
  let units  = '&units=metric';
  url= url+city+"&"+appId;

  request(url, function(error,responce,body){

    weather_json= JSON.parse(body);

       try{
        var weather ={

          //------------Curr------------//
          city: weather_json.city.name,
          temperature: Math.round(weather_json.list[0].main.temp)-273,
          max_temp : Math.round(weather_json.list[0].main.temp_max)-273,
          min_temp : Math.round(weather_json.list[0].main.temp_min)-273,
          description: weather_json.list[0].weather[0].main,
          icon : weather_json.list[0].weather[0].icon,

          //----------------------------------------------------//
          mon_temp : Math.round(weather_json.list[6].main.temp)-273,
          mon_max_temp : Math.round(weather_json.list[6].main.temp_max)-273,
          mon_min_temp : Math.round(weather_json.list[6].main.temp_min)-273,
          mon_desc  : weather_json.list[6].weather[0].description,
          mon_icon : weather_json.list[6].weather[0].icon,
          //------------------------------------------------------//

          thur_temp: Math.round(weather_json.list[14].main.temp)-273,
          thur_max_temp : Math.round(weather_json.list[14].main.temp_max)-273,
          thur_min_temp : Math.round(weather_json.list[14].main.temp_min)-273,
          thur_desc :  weather_json.list[14].weather[0].description,
          thur_icon:   weather_json.list[14].weather[0].icon,
          //-----------------------------------------------------------------------------------//
          wen_temp: Math.round(weather_json.list[22].main.temp)-273,
          wen_max_temp :Math.round(weather_json.list[22].main.temp_max)-273,
          wen_min_temp : Math.round(weather_json.list[22].main.temp_min)-273,
          wen_desc :  weather_json.list[22].weather[0].description,
          wen_icon:   weather_json.list[22].weather[0].icon,
          //-----------------------------------------------------------------------------------//
          tues_temp: Math.round(weather_json.list[30].main.temp)-273,
          tues_max_temp : Math.round(weather_json.list[30].main.temp_max)-273,
          tues_min_temp : Math.round(weather_json.list[30].main.temp_min)-273,
          tues_desc :  weather_json.list[30].weather[0].description,
          tues_icon:   weather_json.list[30].weather[0].icon,
          //-----------------------------------------------------------------------------------//
          fri_temp: Math.round(weather_json.list[38].main.temp)-273,
          fri_max_temp :Math.round(weather_json.list[38].main.temp_max)-273,
          fri_min_temp : Math.round(weather_json.list[38].main.temp_min)-273,
          fri_desc :  weather_json.list[38].weather[0].description,
          fri_icon:   weather_json.list[38].weather[0].icon,
        }
      }
        catch(e){
          res.render('404');
        }
        res.render('index',{weather: weather});

      });

  });
app.use(function(req, res, next){
    res.status(404).render('404');
});
const port = process.env.PORT || 3000

app.listen(port,() => {
  console.log(`Server running at port `+port);
});
